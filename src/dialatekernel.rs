use grid::Grid;


pub fn create_kernel(n: usize) -> Grid<u8> {
    //width = height = 2n+1
    let size = n * 2 + 1;
    let mut kernel: Grid<u8> = Grid::new(size, size);


    // let center = n;
    //iterate through each one, determine distance to center, if within n units, set to 1

    for ((row, col), i) in kernel.indexed_iter_mut() {
        //l1 norm is |x| + |y|
        //usize numbers can be 64 bit, so to prevent overflow, recast to 128 bit for subtraction
        let d = ((row as i128 - n as i128) as i64).abs() + ((col as i128 - n as i128) as i64).abs();
        if d as usize <= n {
            *i = 1;
        }
    }


    kernel
}


pub fn dialate(input: &Grid<u8>, kernel: &Grid<u8>) -> Grid<u8> {
    let mut grid = input.clone();
    let (h, w) = grid.size();

    //get center of kernel:
    let k_size = kernel.cols();
    assert_eq!(k_size % 2, 1);//kernel should be odd-sized
    let n = ((kernel.cols() - 1) / 2) as i128;


    //row, then col: this is best for row major layout, which Grid lib uses
    for row in 0..h {
        for col in 0..w {
            let mut do_op: bool = false;

            //is this element a '1'?
            //this is a one-pass, so read from input grid only to ensure that recently written
            // '1's don't get checked twice
            if let Some(pixel) = input.get(row, col) {
                if *pixel == 1 {
                    do_op = true;
                }
            }

            if do_op {
                //set all pixels to 1 around target pixel that correspond to a '1' in the kernel
                for ((k_row, k_col), i) in kernel.indexed_iter() {
                    if *i == 1 {
                        //set the relative pixel to 1, if possible

                        let rel_row = row as i128 + (k_row as i128 - n);
                        let rel_col = col as i128 + (k_col as i128 - n);
                        if rel_row >= 0 && rel_col >= 0 {
                            if let Some(pixel) = grid.get_mut(rel_row, rel_col) {
                                *pixel = 1;
                            }
                        }
                    }
                }
            }
        }
    }


    grid
}

