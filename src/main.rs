mod dialatekernel;
mod tests;

use std::cmp::{max, min};

use grid::*;


fn main() {

    // want to represent 2d data? typically you use
    // simple c-style row major access:
    //vec[a][b] => vec[dim_b*a + b]

    // but for ease, I'm going to use a library that allows easier 2d vector access: Grid


    //this is normally the spot where I'd grab program args and maybe decode input from a file/image
    //for now, this will be run/tested with hardcoded demo code


    let inputgrid = grid![
            [1,0,0,0,0,0,0,0,0,0,1]
            [0,0,0,0,0,0,0,0,0,0,0]
            [0,0,1,0,0,0,0,0,0,0,0]
            [0,0,0,0,0,0,0,0,0,0,0]
            [0,0,0,0,0,0,0,0,0,0,0]
            [0,0,0,0,0,1,0,0,0,0,0]
            [0,0,0,0,0,0,0,0,0,0,0]
            [0,0,0,0,0,0,0,0,0,0,0]
            [0,0,0,0,0,0,0,0,0,0,0]
            [0,1,0,0,0,0,0,0,0,0,0]
            [1,0,0,0,0,0,0,0,0,1,1]
            ];
    let n = 3;


    // let k = create_kernel(3);

    println!("input:");
    printgrid(&inputgrid);

    println!("with n={n}");

    let result = watershed(&inputgrid, n);

    println!("output:");
    printgrid(&result);

    let count = countgrid(&result);

    println!("total count is: {count}");
}


///watershed
///     parameters:
///     input image (binary image, 0/1), size WxH
///        0 is background/negative. 1 is object/positive
///     distance N>0, representing steps to mark via manhattan distance from any object
///
fn watershed(input: &Grid<u8>, mut n: usize) -> Grid<u8> {
    let mut grid = input.clone();
    let (h, w) = grid.size();

    //pointless to iterate beyond size of input image
    // limit steps to no more than the larger of the two dimensions
    n = min(n, max(h, w));

    //not sure if this is optimal, but it should be able to be parallelized well on CUDA

    for _ in 1..=n { //watershed step
        // println!("step {l}:");

        //need buffer so that neighbor checking doesn't include recently written
        let buffer = grid.clone();
        //read from buffer, write to grid.

        //row, then col: this is best for row major layout, which Grid lib uses
        for row in 0..h {
            for col in 0..w {
                //look at cardinal-4 neighbors. If a neighbor has been filled, fill it.
                // println!("checking neighbors for{row}, {col}");
                if hasneighbor(&buffer, row, col) {
                    // println!("ole!");
                    if let Some(pixel) = grid.get_mut(row, col) {
                        *pixel = 1;
                    }
                }
            }
        }
    }

    grid
}

/// This helper function is for if the image used is positive/negative numbers instead of 1/0
#[allow(dead_code)]
fn convert_posneg_to_binary(posneg: Grid<i8>) -> Grid<u8> {
    let (r, c) = posneg.size();
    let mut binary: Grid<u8> = Grid::new(r, c);


    for ((row, col), i) in posneg.indexed_iter() {
        if *i > 0 {
            if let Some(b) = binary.get_mut(row, col) {
                *b = 1
            }
        } else if let Some(b) = binary.get_mut(row, col) {
            *b = 0
        }
    }

    binary
}


/// check a pixel's 4 neighbors
/// return true if any of them are '1'
fn hasneighbor(grid: &Grid<u8>, row: usize, col: usize) -> bool {
    //check up, down, left, right
    //if oob, default to 0

    //dont underflow
    let u = if row == 0 {
        &0
    } else {
        grid.get(row - 1, col).unwrap_or(&0)
    };

    let l = if col == 0 {
        &0
    } else {
        grid.get(row, col - 1).unwrap_or(&0)
    };

    let d = grid.get(row + 1, col).unwrap_or(&0);
    let r = grid.get(row, col + 1).unwrap_or(&0);

    if u + d + l + r > 0 { //if any of them are 1 (assume only 0 or 1 in image)
        true
    } else {
        false
    }
}

/// nice fancy printing
fn printgrid(grid: &Grid<u8>) {
    let (h, w) = grid.size();
    println!("[");

    for row in 0..h {
        print!("[");
        for col in 0..w {
            if let Some(p) = grid.get(row, col) {
                print!("{p},");
            }
        }
        println!("]")
    }
    println!("]");
}


fn countgrid(grid: &Grid<u8>) -> usize {
    let mut count: usize = 0;

    for i in grid.iter() {
        if *i == 1 {
            count += 1;
        }
    }

    count
}

