This demonstrates a morphological dialation using L1 norm distance of N steps.

Although the term `watershed` is used in the project name,
watershed is used for gradients, and in this application the distance of each pixel is not required to be computed.

Build with

`cargo build`

Then Run with

`cargo run`

To run tests use

`cargo test`